<?php

declare(strict_types=1);

namespace Tests\Unit\App\Repositories;

use App\Entities\PhraseEntity;
use App\Http\Gateways\AlgoliaGateway;
use App\Repositories\AlgoliaPhraseRepository;
use Mockery;
use Tests\SimpleTestCase;

class AlgoliaPhraseRepositoryTest extends SimpleTestCase
{
    public function testCreatePhrase(): void
    {
        $phrase = new PhraseEntity();
        $gateway = Mockery::spy(AlgoliaGateway::class);

        $repository = new AlgoliaPhraseRepository($gateway);
        $repository->createPhrase($phrase);

        $gateway->shouldHaveReceived('addRecord')->with('dictionary', $phrase);
    }

    public function testCreatePhrases(): void
    {
        $gateway = Mockery::spy(AlgoliaGateway::class);

        $repository = new AlgoliaPhraseRepository($gateway);

        $repository->createPhrases([]);

        $gateway->shouldHaveReceived('addRecords')->with('dictionary', []);
    }

    public function testRemoveById(): void
    {
        $gateway = Mockery::spy(AlgoliaGateway::class);

        $repository = new AlgoliaPhraseRepository($gateway);

        $repository->removeById(368);

        $gateway->shouldHaveReceived('deleteObject')->with('dictionary', 368);
    }

    public function testUpdateById(): void
    {
        $gateway = Mockery::spy(AlgoliaGateway::class);

        $repository = new AlgoliaPhraseRepository($gateway);

        $repository->updateById(12, 'cow');

        $gateway->shouldHaveReceived('updateRecord')->with('dictionary', 12, 'cow');
    }
}
