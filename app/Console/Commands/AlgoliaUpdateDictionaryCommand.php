<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Algolia\AlgoliaSearch\SearchClient;
use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Facades\DB;
use stdClass;

final class AlgoliaUpdateDictionaryCommand extends Command
{
    protected $signature = 'algolia:update-dictionary';

    public function __construct(
        private readonly SearchClient $algolia,
        private readonly Config $config
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->output->writeln('Updating dictionary in Algolia...');

        $phrases = DB::table('phrases as p')
            ->addSelect('p.id as phrase_id')
            ->addSelect('d.name as dictionary_name')
            ->join('dictionaries as d', 'p.dictionary_id', '=', 'd.id')
            ->get()
            ->toArray();

        $objects = array_map(static fn (stdClass $row): array => [
            'objectID' => $row->phrase_id,
            'dictionary' => $row->dictionary_name,
        ], $phrases);

        $this->algolia->initIndex($this->config->get('algolia.index_prefix') . '_dictionary')
            ->partialUpdateObjects($objects);

        $this->output->writeln('Complete!');
    }
}
