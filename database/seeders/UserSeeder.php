<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

final class UserSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('users')
            ->insert([
                'username' => 'harry',
                'password' => Hash::make('hello'),
                'is_admin' => true,
                'created_at' => Carbon::now()->toIso8601String(),
            ]);
    }
}
