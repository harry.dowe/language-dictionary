<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\DictionaryEntity;
use App\Entities\PhraseEntity;

interface PhraseRepositoryInterface
{
    public function createPhrase(string $phrase, DictionaryEntity $dictionary): PhraseEntity;

    public function removeById(int $id): void;

    public function updateById(int $id, string $phrase): void;
}
