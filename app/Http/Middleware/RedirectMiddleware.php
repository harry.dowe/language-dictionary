<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Laravel\Lumen\Routing\UrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Redirects requests from the heroku URL to the custom domain.
 */
final class RedirectMiddleware
{
    public function __construct(
        private readonly UrlGenerator $url,
        private readonly string $from,
    ) {
    }

    public function handle(Request $request, Closure $next): RedirectResponse | Response
    {
        if ($this->shouldRedirect($request)) {
            return new RedirectResponse($this->getDestination($request), Response::HTTP_MOVED_PERMANENTLY);
        }

        return $next($request);
    }

    private function shouldRedirect(Request $request): bool
    {
        return $request->getHost() === $this->from;
    }

    private function getDestination(Request $request): string
    {
        $queryString = $request->getQueryString();

        if ($queryString !== null) {
            $queryString = "?{$queryString}";
        }

        return $this->url->to("{$request->getPathInfo()}{$queryString}");
    }
}
