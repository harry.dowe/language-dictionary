<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

final class SetAdminCommand extends Command
{
    protected $signature = 'admin:set {name}';

    public function handle(): void
    {
        DB::table('users')
            ->where('username', $this->input->getArgument('name'))
            ->update([
                'is_admin' => true,
            ]);
    }
}
