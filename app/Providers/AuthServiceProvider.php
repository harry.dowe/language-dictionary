<?php

declare(strict_types=1);

namespace App\Providers;

use App\Auth\DatabaseUserProvider;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Facades\Auth;
use Psr\Container\ContainerInterface;
use Tymon\JWTAuth\Providers\LumenServiceProvider as JwtAuthServiceProvider;

final class AuthServiceProvider extends JwtAuthServiceProvider
{
    public function boot()
    {
        parent::boot();

        Auth::provider('database', static function (ContainerInterface $app, array $config): UserProvider {
            return new DatabaseUserProvider(
                $app->make(ConnectionInterface::class),
                $app->make(Hasher::class),
                $config['table'],
            );
        });
    }
}
