<?php

declare(strict_types=1);

namespace App\Providers;

use Algolia\AlgoliaSearch\Config\SearchConfig;
use Algolia\AlgoliaSearch\SearchClient;
use App\Console\Commands\ImportPhrasesCommand;
use App\Http\Controllers\AuthController;
use App\Http\Gateways\AlgoliaGateway;
use App\Http\Middleware\RedirectMiddleware;
use App\Repositories\DatabasePhraseRepository;
use App\Validation\Request\UniqueDictionary\UniqueDictionary;
use App\Validation\Uri\PhraseExists;
use App\Validation\Request\UniquePhrase;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;
use Psr\Container\ContainerInterface;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property Application $app
 */
final class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Validator::extend('unique_phrase', UniquePhrase::class);
        Validator::extend('unique_dictionary', UniqueDictionary::class);
        Validator::extend('phrase_exists', PhraseExists::class);

        if ($this->app->environment('production')) {
            URL::forceScheme('https');
        }

        URL::forceRootUrl($this->app->get('config')->get('app.url'));
    }

    public function register()
    {
        $this->app->bind(AuthController::class, static function (ContainerInterface $app): AuthController {
            return new AuthController($app->get('auth')->guard('api'));
        });

        $this->app->bind(AlgoliaGateway::class, static function (ContainerInterface $app): AlgoliaGateway {
            return new AlgoliaGateway(
                $app->get(SearchClient::class),
                $app->get('config')->get('algolia.index_prefix'),
            );
        });

        $this->app->bind(ConnectionInterface::class, static function (Application $app): ConnectionInterface {
            return $app->make('db')->connection($app->get('config')->get('database.default'));
        });

        $this->app->bind(UniquePhrase::class, static function (ContainerInterface $app): UniquePhrase {
            return new UniquePhrase($app->get(DatabasePhraseRepository::class));
        });

        $this->app->bind(SearchClient::class, static function (ContainerInterface $app): SearchClient {
            $config = $app->get('config')->get('algolia');
            $searchConfig = SearchConfig::create($config['app_id'], $config['api_key']);

            return SearchClient::createWithConfig($searchConfig);
        });

        $this->app->when(RedirectMiddleware::class)
            ->needs('$from')
            ->give(static function (ContainerInterface $app): string {
                return parse_url($app->get('config')->get('heroku.url'), PHP_URL_HOST);
            });

        $this->app->when(ImportPhrasesCommand::class)
            ->needs('$indexPrefix')
            ->giveConfig('algolia.index_prefix');

        $this->app->bind(JWTSubject::class, Authenticatable::class);
    }
}
