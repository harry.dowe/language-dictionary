<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\PhraseEntity;
use App\Http\Gateways\AlgoliaGateway;

class AlgoliaPhraseRepository
{
    public function __construct(
        private readonly AlgoliaGateway $gateway,
    ) {
    }

    public function createPhrase(PhraseEntity $entity): void
    {
        $this->gateway->addRecord('dictionary', $entity);
    }

    /**
     * @param PhraseEntity[]
     */
    public function createPhrases(array $phrases): void
    {
        $this->gateway->addRecords('dictionary', $phrases);
    }

    public function removeById(int $id): void
    {
        $this->gateway->deleteObject('dictionary', $id);
    }

    public function updateById(int $id, string $phrase): void
    {
        $this->gateway->updateRecord('dictionary', $id, $phrase);
    }
}
