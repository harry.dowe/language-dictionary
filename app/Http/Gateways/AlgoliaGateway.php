<?php

declare(strict_types=1);

namespace App\Http\Gateways;

use Algolia\AlgoliaSearch\SearchClient;
use App\Entities\PhraseEntity;
use Illuminate\Support\Carbon;

class AlgoliaGateway
{
    public function __construct(
        private readonly SearchClient $client,
        private readonly ?string $indexPrefix = null,
    ) {
    }

    private function buildIndex(string $index): string
    {
        if ($this->indexPrefix === null) {
            return $index;
        }

        return "{$this->indexPrefix}_{$index}";
    }

    public function addRecord(string $index, PhraseEntity $entity): void
    {
        $this->client->initIndex($this->buildIndex($index))->saveObject([
            'objectID' => $entity->getId(),
            'phrase' => $entity->getPhrase(),
            'dictionary' => $entity->getDictionary()->getName(),
            'createdAt' => Carbon::now()->unix(),
        ]);
    }

    /**
     * @param PhraseEntity[]  $entities
     */
    public function addRecords(string $index, array $entities): void
    {
        $now = Carbon::now()->unix();

        $this->client->initIndex($this->buildIndex($index))
            ->saveObjects(array_map(static fn (PhraseEntity $entity): array => [
                'objectID' => $entity->getId(),
                'phrase' => $entity->getPhrase(),
                'dictionary' => $entity->getDictionary()->getName(),
                'createdAt' => $now,
            ], $entities));
    }

    public function deleteObject(string $index, int $id): void
    {
        $this->client->initIndex($this->buildIndex($index))
            ->deleteObject($id);
    }

    public function clearObjects(string $index): void
    {
        $this->client->initIndex($this->buildIndex($index))->clearObjects();
    }

    public function updateRecord(string $index, int $id, string $phrase): void
    {
        $this->client->initIndex($this->buildIndex($index))
            ->partialUpdateObject([
                'objectID' => $id,
                'phrase' => $phrase,
            ]);
    }
}
