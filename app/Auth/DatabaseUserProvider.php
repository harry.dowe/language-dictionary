<?php

declare(strict_types=1);

namespace App\Auth;

use Illuminate\Auth\DatabaseUserProvider as BaseProvider;

final class DatabaseUserProvider extends BaseProvider
{
    protected function getGenericUser($user)
    {
        if ($user === null) {
            return null;
        }

        return new User($user->id, $user->password, $user->is_admin);
    }
}
