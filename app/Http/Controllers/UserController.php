<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class UserController extends Controller
{
    public function __construct(
        private readonly UserRepository $repository,
        private readonly ValidationFactory $validationFactory,
    ) {
    }

    public function create(Request $request): JsonResponse
    {
        $this->validationFactory->make($request->json()?->all() ?? [], [
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'between:8,255'],
        ])->validate();

        $this->repository->createUser(
            trim($request->json('username')),
            $request->json('password'),
        );

        return new JsonResponse(status: Response::HTTP_CREATED);
    }
}
