<?php

declare(strict_types=1);

namespace Tests\Unit\App\Http\Middleware;

use App\FeatureToggle\Feature;
use App\FeatureToggle\FeatureEnabledService;
use App\Http\Middleware\FeatureEnabledMiddleware;
use InvalidArgumentException;
use Mockery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tests\SimpleTestCase;

class FeatureEnabledMiddlewareTest extends SimpleTestCase
{
    public function testThrowsExceptionWhenFeatureDisabled(): void
    {
        $service = Mockery::mock(FeatureEnabledService::class);
        $service->shouldReceive('featureEnabled')
            ->with(Feature::CreateUsers)
            ->andReturn(false);

        $middleware = new FeatureEnabledMiddleware($service);

        $this->expectException(AccessDeniedHttpException::class);
        $middleware->handle(new Request(), static function () {
        }, 'sheep.create');
    }

    public function testReturnsNextRequestWhenFeatureEnabled(): void
    {
        $service = Mockery::mock(FeatureEnabledService::class);
        $service->shouldReceive('featureEnabled')
            ->with(Feature::CreateUsers)
            ->andReturn(true);

        $middleware = new FeatureEnabledMiddleware($service);

        $request = new Request(content: 'baa');
        $response = $middleware->handle(
            $request,
            static fn (Request $request): Response => new Response($request->getContent()),
            'sheep.create',
        );

        $this->assertSame('baa', $response->getContent());
    }
}
