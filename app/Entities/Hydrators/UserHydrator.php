<?php

declare(strict_types=1);

namespace App\Entities\Hydrators;

use App\Entities\UserEntity;
use DateTimeInterface;

class UserHydrator
{
    public static function hydrate(
        int $id,
        string $username,
        string $password,
        bool $isAdmin,
        ?DateTimeInterface $createdAt = null,
        ?DateTimeInterface $updatedAt = null,
    ): UserEntity {
        $user = new UserEntity();
        $user->setId($id);
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setIsAdmin($isAdmin);
        $user->setCreatedAt($createdAt);
        $user->setUpdatedAt($updatedAt);

        return $user;
    }
}
