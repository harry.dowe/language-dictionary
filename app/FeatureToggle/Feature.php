<?php

declare(strict_types=1);

namespace App\FeatureToggle;

enum Feature: string
{
    case CreateUsers = 'users.create';
}
