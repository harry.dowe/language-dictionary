mix:
	npm run development

watch:
	npm run watch

build:
	docker build \
		--target=prod \
		-t registry.gitlab.com/harry.dowe/language-dictionary:latest \
		-t registry.gitlab.com/harry.dowe/language-dictionary:$(shell git rev-parse HEAD) \
		--target prod
		.

	docker push registry.gitlab.com/harry.dowe/language-dictionary
	docker push registry.gitlab.com/harry.dowe/language-dictionary:$(shell git rev-parse HEAD)

deploy:
	git push heroku master

migrate-create:
	docker-compose exec \
		--user=$(shell id -g):$(shell id -u) \
		web php artisan make:migration $(name)

migrate:
	docker-compose exec web php artisan migrate

migrate-rollback:
	docker-compose exec web php artisan migrate:rollback

migrate-install:
	docker-compose exec web php artisan migrate:install

sync:
	docker-compose exec web php artisan sync

seed:
	docker-compose exec web php artisan db:seed --class=$(class)

import:
	docker-compose exec web php artisan import

list-users:
	docker-compose exec web php artisan admin:users:list

list-dictionaries:
	docker-compose exec web php artisan admin:dictionaries:list

fmt:
	docker-compose exec web php vendor/bin/php-cs-fixer fix

dictionary-ownership:
	docker-compose exec web php artisan dictionary_ownership:set $(dictionary) $(username)

prod-shell:
	heroku run sh

prod-registration:
	heroku config:set CREATE_USERS_ENABLED=$(value)

composer-install:
	docker run --rm --interactive --tty \
		--volume ${PWD}:/app \
		--volume ${HOME}/.composer:/tmp \
		--user $(shell id -u):$(shell id -g) \
		composer:2.0.11 install --ignore-platform-reqs

test:
	docker-compose exec web php vendor/bin/phpunit

proxy:
	lt --port 6060

import-dump:
	docker cp $(path) $(shell docker-compose ps -q db):/backup
	docker-compose exec db pg_restore -d language-dictionary /backup -c -U root
