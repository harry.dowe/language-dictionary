<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
    ->in('app')
    ->in('bootstrap')
    ->in('config')
    ->in('tests')
    ->in('database')
    ->exclude('vendor')
    ->exclude('node_modules')
    ->exclude('resources')
    ->exclude('storage');

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR12' => true,
        'array_syntax' => true,
        'binary_operator_spaces' => true,
        'cast_spaces' => true,
        'combine_consecutive_unsets' => true,
        'concat_space' => ['spacing' => 'one'],
        'no_blank_lines_after_class_opening' => true,
        'no_trailing_comma_in_singleline_array' => true,
        'no_whitespace_in_blank_line' => true,
        'no_spaces_around_offset' => true,
        'no_unused_imports' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'no_whitespace_before_comma_in_array' => true,
        'normalize_index_brace' => true,
        'no_blank_lines_after_phpdoc' => true,
        'no_superfluous_phpdoc_tags' => true,
        'phpdoc_indent' => true,
        'phpdoc_to_comment' => true,
        'phpdoc_trim' => true,
        'phpdoc_scalar' => true,
        'phpdoc_no_empty_return' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_no_package' => true,
        'no_empty_phpdoc' => true,
        'phpdoc_trim_consecutive_blank_line_separation' => true,
        'phpdoc_separation' => true,
        'single_quote' => true,
        'ternary_to_null_coalescing' => true,
        'trailing_comma_in_multiline' => ['elements' => ['arrays', 'arguments', 'parameters']],
        'trim_array_spaces' => true,
        'method_argument_space' => ['on_multiline' => 'ensure_fully_multiline'],
        'no_break_comment' => false,
        'blank_line_before_statement' => true,
        'return_assignment' => true,
        'php_unit_test_case_static_method_calls' => ['call_type' => 'this'],
        'php_unit_test_annotation' => false,
        'php_unit_strict' => true,
        'php_unit_method_casing' => true,
        'object_operator_without_whitespace' => true,
        'no_unneeded_control_parentheses' => true,
        'no_superfluous_elseif' => true,
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_leading_namespace_whitespace' => true,
        'no_leading_import_slash' => true,
        'no_empty_statement' => true,
        'multiline_whitespace_before_semicolons' => true,
        'method_chaining_indentation' => true,
        'lowercase_static_reference' => true,
        'lowercase_cast' => true,
        'list_syntax' => ['syntax' => 'short'],
        'heredoc_indentation' => true,
        'array_indentation' => true,
        'strict_comparison' => true,
        'space_after_semicolon' => true,
        'single_blank_line_before_namespace' => true,
        'return_type_declaration' => true,
        'no_extra_blank_lines' => [
            'tokens' => ['break', 'continue', 'curly_brace_block', 'return', 'throw', 'use', 'use_trait'],
        ],
        'clean_namespace' => true,
        'no_unset_cast' => true,
        'simple_to_complex_string_variable' => true,
        'explicit_string_variable' => true,
        'new_with_braces' => true,
    ])
    ->setFinder($finder);
