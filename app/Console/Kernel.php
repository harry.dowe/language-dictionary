<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\AlgoliaUpdateDictionaryCommand;
use App\Console\Commands\CopyIndexSettingsCommand;
use App\Console\Commands\DictionaryOwnershipCommand;
use App\Console\Commands\ImportPhrasesCommand;
use App\Console\Commands\ListDictionariesCommand;
use App\Console\Commands\ListUsersCommand;
use App\Console\Commands\SetAdminCommand;
use App\Console\Commands\SyncPhrasesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

final class Kernel extends ConsoleKernel
{
    /**
     * @var string[]
     */
    protected $commands = [
        CopyIndexSettingsCommand::class,
        SyncPhrasesCommand::class,
        ImportPhrasesCommand::class,
        AlgoliaUpdateDictionaryCommand::class,
        SetAdminCommand::class,
        DictionaryOwnershipCommand::class,
        ListUsersCommand::class,
        ListDictionariesCommand::class,
    ];

    protected function schedule(Schedule $schedule)
    {
    }
}
