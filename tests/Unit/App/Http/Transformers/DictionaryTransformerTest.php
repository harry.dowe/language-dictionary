<?php

declare(strict_types=1);

namespace Tests\Unit\App\Http\Transformers;

use App\Entities\DictionaryEntity;
use App\Entities\UserEntity;
use App\Http\Transformers\DictionaryTransformer;
use Tests\SimpleTestCase;

class DictionaryTransformerTest extends SimpleTestCase
{
    public function testExtract(): void
    {
        $dictionary = new DictionaryEntity();
        $dictionary->setId(34);
        $dictionary->setName('German');
        $owner = new UserEntity();
        $owner->setId(45);

        $dictionary->setOwner($owner);

        $result = DictionaryTransformer::extract($dictionary);

        $this->assertSame([
            'id' => 34,
            'name' => 'German',
            'owner' => 45,
        ], $result);
    }
}
