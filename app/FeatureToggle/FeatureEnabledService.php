<?php

declare(strict_types=1);

namespace App\FeatureToggle;

use Illuminate\Contracts\Config\Repository;
use UnexpectedValueException;

class FeatureEnabledService
{
    public function __construct(
        private readonly Repository $config
    ) {
    }

    public function canCreateUsers(): bool
    {
        return $this->featureEnabled(Feature::CreateUsers);
    }

    /**
     * @throws UnexpectedValueException
     */
    public function featureEnabled(Feature $feature): bool
    {
        $value = $this->config->get("features.{$feature->value}");

        if ($value === null) {
            throw new UnexpectedValueException("Could not find feature `features.{$feature->value}`");
        }

        return $value;
    }
}
