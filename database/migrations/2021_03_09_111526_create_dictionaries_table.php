<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

final class CreateDictionariesTable extends Migration
{
    public function up(): void
    {
        Schema::create('dictionaries', static function (Blueprint $table): void {
            $table->id();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => 'DictionarySeeder',
            '--force' => true,
        ]);
    }

    public function down(): void
    {
        Schema::dropIfExists('dictionaries');
    }
}
