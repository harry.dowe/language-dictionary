<?php

declare(strict_types=1);

namespace App\Entities;

class PhraseEntity
{
    private int $id;

    private string $phrase;

    private DictionaryEntity $dictionary;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function setPhrase(string $phrase): void
    {
        $this->phrase = $phrase;
    }

    public function getDictionary(): DictionaryEntity
    {
        return $this->dictionary;
    }

    public function setDictionary(DictionaryEntity $dictionary): void
    {
        $this->dictionary = $dictionary;
    }
}
