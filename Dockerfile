FROM node:18.8.0-alpine3.16 as node

ARG MIX_ALGOLIA_INDEX_PREFIX='prod'
ARG MIX_ALGOLIA_APP_ID
ARG MIX_ALGOLIA_API_KEY
ARG MIX_APP_URL

WORKDIR /home/node/app

COPY . /home/node/app

RUN echo 'update-notifier=false' > /home/node/app/.npmrc \
    && npm install --no-audit --no-fund --ignore-scripts \
    && npm run production

FROM php:8.1.9-fpm-alpine3.16 as prod

WORKDIR /var/www/html

RUN apk update && apk upgrade \
    && apk add --no-cache $PHPIZE_DEPS \
    && apk add nginx \
    && apk add postgresql-dev \
    && docker-php-ext-install pdo pdo_pgsql \
    && mkdir -p /run/nginx \
    && sed -i -E "s/error_log .+/error_log \/dev\/stderr warn;/" /etc/nginx/nginx.conf \
    && sed -i -E "s/access_log .+/access_log \/dev\/stdout main;/" /etc/nginx/nginx.conf \
    && apk add supervisor \
    && mkdir -p /etc/supervisor.d/ \
    && rm -rf /var/cache/apk/*

COPY --from=composer:2.4.1 /usr/bin/composer /usr/bin/composer

COPY --from=node /home/node/app/public/js /var/www/html/public/js
COPY --from=node /home/node/app/public/css /var/www/html/public/css
COPY --from=node /home/node/app/public/fonts /var/www/html/public/fonts

ENV NGINX_CONFD_DIR /etc/nginx/conf.d

COPY docker/php.ini $PHP_INI_DIR/
COPY docker/nginx.conf $NGINX_CONFD_DIR/default.conf
COPY docker/supervisor.programs.ini /etc/supervisor.d/
COPY docker/start.sh /

RUN mkdir -p /var/www/html/storage/framework/cache/data \
    && mkdir -p /var/www/html/storage/framework/views \
    && adduser -D nonroot \
    && chmod a+x /start.sh \
    && chmod -R a+w /etc/nginx \
    && chmod -R a+w /run/nginx \
    && chmod -R a+r /etc/supervisor* \
    && sed -i -E "s/^file=\/run\/supervisord\.sock/file=\/run\/supervisord\/supervisord.conf/" /etc/supervisord.conf \
    && mkdir -p /run/supervisord \
    && chmod -R a+w /run/supervisord \
    && chmod -R a+w /var/log \
    && apk add --update sudo \
    && echo "nonroot ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

COPY . /var/www/html

RUN composer install \
    --prefer-dist \
    --no-dev \
    --no-interaction \
    --ignore-platform-reqs \
    --no-scripts \
    --no-progress \
    --optimize-autoloader

RUN chmod -R a+w /var/www/html

CMD ["/start.sh"]

FROM prod as dev

RUN apk add autoconf g++ make && \
    pecl install xdebug-3.1.5 && \
    docker-php-ext-enable xdebug && \
    echo 'xdebug.idekey = "xdebug"' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.mode=debug' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.start_with_request=yes' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.client_host=172.17.0.1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.client_port=9003' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
