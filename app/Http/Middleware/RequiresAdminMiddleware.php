<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tymon\JWTAuth\Contracts\JWTSubject;

final class RequiresAdminMiddleware
{
    public function __construct(
        private readonly JWTSubject $user,
    ) {
    }

    public function handle(Request $request, Closure $next): Response
    {
        if (!$this->isAdmin()) {
            throw new AccessDeniedHttpException('Requires admin');
        }

        return $next($request);
    }

    private function isAdmin(): bool
    {
        return $this->user->getJWTCustomClaims()['admin'] ?? false;
    }
}
