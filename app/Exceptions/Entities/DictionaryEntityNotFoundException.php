<?php

declare(strict_types=1);

namespace App\Exceptions\Entities;

class DictionaryEntityNotFoundException extends EntityNotFoundException
{
}
