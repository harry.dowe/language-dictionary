<?php

declare(strict_types=1);

return [
    'default' => 'pgsql',

    'connections' => [
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => parse_url(env('DATABASE_URL'), PHP_URL_HOST),
            'port' => (int) parse_url(env('DATABASE_URL'), PHP_URL_PORT),
            'database' => ltrim(parse_url(env('DATABASE_URL'), PHP_URL_PATH), '/'),
            'username' => parse_url(env('DATABASE_URL'), PHP_URL_USER),
            'password' => parse_url(env('DATABASE_URL'), PHP_URL_PASS),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],
    ],

    'migrations' => 'migrations',
];
