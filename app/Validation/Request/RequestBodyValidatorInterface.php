<?php

declare(strict_types=1);

namespace App\Validation\Request;

use Illuminate\Contracts\Validation\Validator;

interface RequestBodyValidatorInterface
{
    public function validate(string $attribute, mixed $value, array $customAttributes, Validator $validator): bool;
}
