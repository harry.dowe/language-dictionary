<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

final class AlterPhrasesAddDictionary extends Migration
{
    public function up(): void
    {
        Schema::table('phrases', static function (Blueprint $table): void {
            $table->foreignId('dictionary_id')->nullable(true);
            $table->foreign('dictionary_id')->references('id')->on('dictionaries');
            $table->dropUnique('phrases_phrase_unique');
            $table->unique(['dictionary_id', 'phrase']);
        });

        DB::table('phrases')->update([
            'dictionary_id' => $this->getDictionary('Spanish'),
        ]);

        Schema::table('phrases', static function (Blueprint $table): void {
            $table->foreignId('dictionary_id')->nullable(false)->change();
        });
    }

    public function down(): void
    {
        Schema::table('phrases', static function (Blueprint $table): void {
            $table->dropForeign('phrases_dictionary_id_foreign');
            $table->dropColumn('dictionary_id');
        });
    }

    private function getDictionary(string $name): int
    {
        return (int) DB::table('dictionaries')
            ->select('id')
            ->where('name', $name)
            ->first('id')
            ->id;
    }
}
