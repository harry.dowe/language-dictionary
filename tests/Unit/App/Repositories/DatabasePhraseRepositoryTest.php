<?php

declare(strict_types=1);

namespace Tests\Unit\App\Repositories;

use App\Entities\DictionaryEntity;
use App\Repositories\DatabasePhraseRepository;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Mockery;
use Mockery\MockInterface;
use Tests\SimpleTestCase;

class DatabasePhraseRepositoryTest extends SimpleTestCase
{
    private ConnectionInterface | MockInterface $db;

    protected function setUp(): void
    {
        $this->db = Mockery::mock(ConnectionInterface::class);
    }

    public function testCreate(): void
    {
        Carbon::setTestNow('2021-03-24T18:44:08+00:00');

        $table = Mockery::mock(Builder::class);
        $table->shouldReceive('insertGetId')->with([
            'phrase' => 'elephant',
            'dictionary_id' => 77,
            'created_at' => '2021-03-24T18:44:08+00:00',
        ])->andReturn(12);

        $this->db->shouldReceive('table')->with('phrases')->andReturn($table);

        $repository = new DatabasePhraseRepository($this->db);

        $dictionary = new DictionaryEntity();
        $dictionary->setId(77);

        $phrase = $repository->createPhrase('elephant', $dictionary);

        $this->assertSame(12, $phrase->getId());
        $this->assertSame('elephant', $phrase->getPhrase());
        $this->assertSame(77, $phrase->getDictionary()->getId());
    }

    public function testRemoveById(): void
    {
        $table = Mockery::spy();

        $this->db->shouldReceive('table')->with('phrases')->andReturn($table);

        $repository = new DatabasePhraseRepository($this->db);
        $repository->removeById(45);

        $table->shouldHaveReceived('delete')->with(45);
    }

    public function testUpdateById(): void
    {
        Carbon::setTestNow('2020-03-24T18:55:13+00:00');

        $table = Mockery::spy(Builder::class);
        $table->shouldReceive('update')->andReturnSelf();
        $table->shouldReceive('where')->andReturnSelf();

        $this->db->shouldReceive('table')->with('phrases')->andReturn($table);

        $repository = new DatabasePhraseRepository($this->db);
        $repository->updateById(29, 'cockatoo');

        $table->shouldHaveReceived('where')->with('id', 29);

        $table->shouldHaveReceived('update')->with([
            'phrase' => 'cockatoo',
            'updated_at' => '2020-03-24T18:55:13+00:00',
        ]);
    }
}
