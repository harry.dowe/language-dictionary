<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Utils\StringValue;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\JWTGuard;

final class AuthController extends Controller
{
    public function __construct(
        private readonly JWTGuard $auth,
    ) {
        $this->middleware('jwt.auth', ['except' => ['login']]);
    }

    public function login(Request $request): JsonResponse
    {
        $token = $this->auth->attempt([
            'username' => StringValue::toTrimLower($request->json('username')),
            'password' => $request->json('password'),
        ]);

        if ($token === false) {
            throw new UnauthorizedHttpException('Bearer realm=api', 'Wrong password.');
        }

        return $this->respondWithToken($token);
    }

    public function refresh(): JsonResponse
    {
        return $this->respondWithToken($this->auth->refresh());
    }

    private function respondWithToken(string $token): JsonResponse
    {
        return new JsonResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->auth->factory()->getTTL() * 60,
        ]);
    }
}
