<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\DictionaryEntity;
use App\Entities\Hydrators\DateTimeHydrator;
use App\Entities\Hydrators\DictionaryHydrator;
use App\Entities\Hydrators\UserHydrator;
use App\Exceptions\Entities\DictionaryEntityNotFoundException;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Carbon;
use stdClass;

class DictionaryRepository
{
    public function __construct(
        private readonly ConnectionInterface $db,
    ) {
    }

    /**
     * @return DictionaryEntity[]
     */
    public function findAll(): array
    {
        $dictionaries = $this->db->table('dictionaries')
            ->orderBy('created_at')
            ->get()
            ->toArray();

        return DictionaryHydrator::hydrateFromArray($dictionaries);
    }

    public function createDictionary(string $name): DictionaryEntity
    {
        $id = $this->db->table('dictionaries')
            ->insertGetId([
                'name' => $name,
                'created_at' => Carbon::now()->toIso8601String(),
                'owner_id' => null,
            ]);

        return DictionaryHydrator::hydrate($id, $name);
    }

    public function existsByParams(?int $id = null, ?string $name = null): bool
    {
        $qb = $this->db->table('dictionaries');

        if ($id !== null) {
            $qb->where('id', $id);
        }

        if ($name !== null) {
            $qb->where('name', $name);
        }

        return $qb->exists();
    }

    public function findById(int $id): DictionaryEntity
    {
        $row = $this->db->table('dictionaries')
            ->where('id', $id)
            ->first();

        if ($row === null) {
            throw new DictionaryEntityNotFoundException("Dictionary with id `{$id}`");
        }

        return DictionaryHydrator::hydrateFromObject($row);
    }

    /**
     * @return DictionaryEntity[]
     */
    public function getDictionaries(): array
    {
        return $this->db->table('dictionaries', 'd')
            ->orderBy('d.created_at')
            ->leftJoin('users as u', 'd.owner_id', '=', 'u.id')
            ->get([
                'd.id as dictionary_id',
                'd.name as dictionary_name',
                'u.id as owner_id',
                'u.username as owner_username',
                'u.password as owner_password',
                'u.is_admin as owner_is_admin',
                'u.created_at as owner_created_at',
                'u.updated_at as owner_updated_at',
            ])
            ->map(static function (stdClass $row): DictionaryEntity {
                $dictionary = DictionaryHydrator::hydrate(
                    $row->dictionary_id,
                    $row->dictionary_name,
                );

                if ($row->owner_id !== null) {
                    $dictionary->setOwner(UserHydrator::hydrate(
                        $row->owner_id,
                        $row->owner_username,
                        $row->owner_password,
                        $row->owner_is_admin,
                        DateTimeHydrator::hydrate($row->owner_created_at),
                        DateTimeHydrator::hydrate($row->owner_updated_at),
                    ));
                }

                return $dictionary;
            })->toArray();
    }
}
