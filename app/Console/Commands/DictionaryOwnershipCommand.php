<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DictionaryOwnershipCommand extends Command
{
    protected $signature = 'dictionary_ownership:set {dictionary} {owner}';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $username = $this->input->getArgument('owner');
        $owner = $this->getUserId($username);

        if ($owner === null) {
            throw new Exception("Couldn't find user with username `{$username}`.");
        }

        $dictionary = $this->input->getArgument('dictionary');
        if (!$this->hasDictionary($dictionary)) {
            throw new Exception("Couldn't find dictionary with name `{$dictionary}`.");
        }

        DB::table('dictionaries')
            ->where('name', $dictionary)
            ->update([
                'owner_id' => $owner,
            ]);

        $this->output->writeln("Set `{$username}` as the owner for the dictionary `{$dictionary}`.");
    }

    private function hasDictionary(string $dictionary): bool
    {
        return DB::table('dictionaries')
            ->where('name', $dictionary)
            ->exists();
    }

    private function getUserId(string $username): ?int
    {
        return DB::table('users')
            ->select('id')
            ->where('username', $username)
            ->first()
            ->id ?? null;
    }
}
