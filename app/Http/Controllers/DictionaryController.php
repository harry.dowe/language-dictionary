<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\DictionaryEntity;
use App\Http\Transformers\DictionaryTransformer;
use App\Repositories\DictionaryRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class DictionaryController extends Controller
{
    public function __construct(
        private DictionaryRepository $repository,
    ) {
    }

    public function index(): JsonResponse
    {
        $dictionaries = array_map(static function (DictionaryEntity $dictionary): array {
            return DictionaryTransformer::extract($dictionary);
        }, $this->repository->findAll());

        return new JsonResponse([
            'dictionaries' => $dictionaries,
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->getValidationFactory()->make($request->json()?->all() ?? [], [
            'name' => [
                'required',
                'string',
                'max:255',
                'unique_dictionary',
            ],
        ])->validate();

        $phrase = trim($request->json('name'));

        $dictionary = $this->repository->createDictionary($phrase);

        return new JsonResponse(DictionaryTransformer::extract($dictionary), Response::HTTP_CREATED);
    }
}
