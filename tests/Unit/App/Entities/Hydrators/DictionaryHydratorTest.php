<?php

declare(strict_types=1);

namespace Tests\Unit\App\Entities\Hydrators;

use App\Entities\Hydrators\DictionaryHydrator;
use stdClass;
use Tests\SimpleTestCase;

class DictionaryHydratorTest extends SimpleTestCase
{
    public function testHydrate(): void
    {
        $result = DictionaryHydrator::hydrate(3, 'French', 45);

        $this->assertSame(3, $result->getId());
        $this->assertSame('French', $result->getName());
        $this->assertSame(45, $result->getOwner()->getId());
    }

    public function testHydrateFromObject(): void
    {
        $object = new stdClass();
        $object->id = 7;
        $object->name = 'German';
        $object->owner_id = 77;

        $result = DictionaryHydrator::hydrateFromObject($object);

        $this->assertSame(7, $result->getId());
        $this->assertSame('German', $result->getName());
        $this->assertSame(77, $result->getOwner()->getId());
    }

    public function testHydrateFromArray(): void
    {
        $object = new stdClass();
        $object->id = 7;
        $object->name = 'German';
        $object->owner_id = 77;

        $result = DictionaryHydrator::hydrateFromArray([
            $object,
        ]);

        $this->assertCount(1, $result);

        $entity = $result[0];

        $this->assertSame(7, $entity->getId());
        $this->assertSame('German', $entity->getName());
        $this->assertSame(77, $entity->getOwner()->getId());
    }
}
