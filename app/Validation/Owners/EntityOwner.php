<?php

declare(strict_types=1);

namespace App\Validation\Owners;

use App\Entities\Interfaces\OwnsResource;
use Tymon\JWTAuth\Contracts\JWTSubject;

class EntityOwner
{
    public static function ownsEntity(JWTSubject $user, OwnsResource $entity): bool
    {
        if ($entity->getOwner() === null) {
            return true;
        }

        if ($user->getJWTCustomClaims()['admin'] ?? false) {
            return true;
        }

        return $user->getAuthIdentifier() === $entity->getOwner()->getId();
    }
}
