<?php

declare(strict_types=1);

namespace Tests\Unit\App\Http\Middleware;

use App\Auth\User;
use App\Http\Middleware\RequiresAdminMiddleware;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tests\SimpleTestCase;

class RequiresAdminMiddlewareTest extends SimpleTestCase
{
    public function testAccessDeniedWhenNotAdmin(): void
    {
        $user = new User(1, 'pass', false);

        $middleware = new RequiresAdminMiddleware($user);

        $this->expectException(AccessDeniedHttpException::class);
        $middleware->handle(new Request(), static function () {
        });
    }

    public function testAccessGrantedWhenAdmin(): void
    {
        $user = new User(1, 'pass', true);

        $middleware = new RequiresAdminMiddleware($user);

        $response = new Response();
        $result = $middleware->handle(new Request(), static fn (Request $request): Response => $response);

        $this->assertSame($response, $result);
    }
}
