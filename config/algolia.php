<?php

declare(strict_types=1);

return [
    'app_id' => env('ALGOLIA_APP_ID'),
    'api_key' => env('ALGOLIA_API_KEY'),
    'index_prefix' => env('ALGOLIA_INDEX_PREFIX', 'dev'),
];
