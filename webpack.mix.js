const mix = require('laravel-mix');

Mix.manifest.refresh = _ => void 0;

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css', {
        sassOptions: {
            includePaths:  ['./node_modules'],
        },
    })
    .disableNotifications();
