<?php

declare(strict_types=1);

namespace App\Validation\Uri;

use App\Repositories\DictionaryRepository;

class DictionaryExists implements RequestUriValidatorInterface
{
    public function __construct(
        private readonly DictionaryRepository $repository,
    ) {
    }

    public function validate(string $value): bool
    {
        return $this->repository->existsByParams(id: (int) $value);
    }
}
