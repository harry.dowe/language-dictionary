<?php

declare(strict_types=1);

namespace App\Validation\Uri;

interface RequestUriValidatorInterface
{
    public function validate(string $value): bool;
}
