<?php

declare(strict_types=1);

namespace Tests\Unit\App\Entities\Hydrators;

use App\Entities\Hydrators\UserHydrator;
use DateTimeImmutable;
use DateTimeInterface;
use Tests\SimpleTestCase;

class UserHydratorTest extends SimpleTestCase
{
    public function testHydrate(): void
    {
        $createdAt = new DateTimeImmutable('1988-03-09 14:30:03');
        $updatedAt = new DateTimeImmutable('1999-05-12 08:10:10');

        $result = UserHydrator::hydrate(3, 'Gary', 'pass', false, $createdAt, $updatedAt);

        $this->assertSame(3, $result->getId());
        $this->assertSame('Gary', $result->getUsername());
        $this->assertSame('pass', $result->getPassword());
        $this->assertFalse($result->isAdmin());

        $this->assertInstanceOf(DateTimeInterface::class, $result->getCreatedAt());
        $this->assertSame('1988-03-09 14:30:03', $createdAt->format('Y-m-d H:i:s'));

        $this->assertInstanceOf(DateTimeInterface::class, $result->getUpdatedAt());
        $this->assertSame('1999-05-12 08:10:10', $updatedAt->format('Y-m-d H:i:s'));
    }

    public function testHydrateNullDateTimes(): void
    {
        $result = UserHydrator::hydrate(3, 'Gary', 'pass', false);

        $this->assertNull($result->getCreatedAt());
        $this->assertNull($result->getUpdatedAt());
    }
}
