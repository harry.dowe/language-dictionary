<?php

declare(strict_types=1);

namespace Tests;

use Mockery;
use PHPUnit\Framework\TestCase;

class SimpleTestCase extends TestCase
{
    protected function tearDown(): void
    {
        $container = Mockery::getContainer();

        if ($container !== null) {
            $this->addToAssertionCount($container->mockery_getExpectationCount());
        }

        Mockery::close();
    }
}
