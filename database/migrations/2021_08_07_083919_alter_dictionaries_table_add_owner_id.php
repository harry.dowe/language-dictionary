<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDictionariesTableAddOwnerId extends Migration
{
    public function up(): void
    {
        Schema::table('dictionaries', static function (Blueprint $table): void {
            $table->foreignId('owner_id')->nullable(true)->default(null);
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    public function down(): void
    {
        Schema::table('dictionaries', static function (Blueprint $table): void {
            $table->dropForeign('dictionaries_owner_id_foreign');
            $table->dropColumn('owner_id');
        });
    }
}
