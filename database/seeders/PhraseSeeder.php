<?php

declare(strict_types=1);

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

final class PhraseSeeder extends Seeder
{
    private readonly Generator $faker;

    private readonly string $now;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->now = Carbon::now()->toIso8601String();
    }

    public function run(): void
    {
        for ($i = 0; $i < 150; $i++) {
            DB::table('phrases')
                ->insert([
                    'phrase' => Str::between($this->faker->unique()->word(), 0, 30),
                    'created_at' => $this->now,
                    'dictionary_id' => 1,
                ]);
        }
    }
}
