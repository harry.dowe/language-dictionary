<?php

declare(strict_types=1);

namespace Tests\Unit\App\Entities\Hydrators;

use App\Entities\Hydrators\DateTimeHydrator;
use DateTimeInterface;
use Tests\SimpleTestCase;

class DateTimeHydratorTest extends SimpleTestCase
{
    public function testHydrate(): void
    {
        $result = DateTimeHydrator::hydrate('2020-03-08 13:56:12');
        $this->assertInstanceOf(DateTimeInterface::class, $result);

        $this->assertSame('2020-03-08 13:56:12', $result->format('Y-m-d H:i:s'));
    }

    public function testHydrateNull(): void
    {
        $this->assertNull(DateTimeHydrator::hydrate(null));
    }
}
