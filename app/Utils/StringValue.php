<?php

declare(strict_types=1);

namespace App\Utils;

use Illuminate\Support\Str;

class StringValue
{
    public static function toTrimLower(string $phrase): string
    {
        return Str::lower(trim($phrase));
    }
}
