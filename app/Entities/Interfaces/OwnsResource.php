<?php

declare(strict_types=1);

namespace App\Entities\Interfaces;

use App\Entities\UserEntity;

interface OwnsResource
{
    public function getOwner(): ?UserEntity;
}
