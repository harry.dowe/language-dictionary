import Vue from 'vue';
import App from './App.vue';
import InstantSearch from 'vue-instantsearch';
import axios from 'axios';
import CopyButton from './components/CopyButton';
import PhraseHit from './components/PhraseHit';
import LoginForm from './components/LoginForm';
import AisLogo from './components/AisLogo';
import Toasted from 'vue-toasted';
import SingularRefinementList from './components/SingularRefinementList';
import RefinementList from './components/RefinementList';
import MaterialTooltip from './components/material/MaterialTooltip';
import ToolbarButton from './components/ToolbarButton';
import MaterialButton from './components/material/MaterialButton';
import MaterialDialog from './components/material/MaterialDialog';
import UploadButton from './components/UploadButton';
import MaterialTextfield from './components/material/MaterialTextfield';
import MaterialField from './components/material/MaterialField';
import SearchBox from './components/SearchBox';
import VueInstantsearchRefresh from './components/VueInstantsearchRefresh';
import MaterialTextfieldIcon from './components/material/MaterialTextfieldIcon';
import SvgIcon from './components/fontawesome/SvgIcon';
import CreateDictionaryButton from "./components/CreateDictionaryButton";

window.Vue = Vue;
window.axios = axios;

axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api/`;
axios.defaults.headers = {
    'Accept': 'application/json',
};

axios.interceptors.request.use(config => {
    const jwt = localStorage.getItem('jwt');

    if (jwt) {
        config.headers['Authorization'] = `Bearer ${jwt}`;
    }

    return config;
});

Vue.use(InstantSearch);

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('CopyButton', CopyButton);
Vue.component('PhraseHit', PhraseHit);
Vue.component('LoginForm', LoginForm);
Vue.component('AisLogo', AisLogo);
Vue.component('SingularRefinementList', SingularRefinementList);
Vue.component('RefinementList', RefinementList);
Vue.component('MaterialTooltip', MaterialTooltip);
Vue.component('ToolbarButton', ToolbarButton);
Vue.component('MaterialButton', MaterialButton);
Vue.component('MaterialDialog', MaterialDialog);
Vue.component('UploadButton', UploadButton);
Vue.component('MaterialTextfield', MaterialTextfield);
Vue.component('MaterialField', MaterialField);
Vue.component('SearchBox', SearchBox);
Vue.component('VueInstantsearchRefresh', VueInstantsearchRefresh);
Vue.component('MaterialTextfieldIcon', MaterialTextfieldIcon);
Vue.component('SvgIcon', SvgIcon);
Vue.component('CreateDictionaryButton', CreateDictionaryButton);

Vue.use(Toasted, {
    iconPack: 'fontawesome',
    theme: 'outline',
    duration: 3000,
    position: 'top-center',
});

Vue.toasted.register('apiError', error => {
    if (error.response) {
        if (error.response.status === 422) {
            return error.response.data.errors[Object.keys(error.response.data.errors)[0]];
        }

        return error.response.data.message;
    }

    return error.message;
}, {
    type: 'error',
    icon: 'exclamation-circle',
});

Vue.toasted.register('error', message => message, {
    type: 'error',
    icon: 'exclamation-circle',
});

const app = new Vue({
    el: '#app',
    render: h => h(App),
});
