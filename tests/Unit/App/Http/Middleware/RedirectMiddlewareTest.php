<?php

declare(strict_types=1);

namespace Tests\Unit\App\Http\Middleware;

use App\Http\Middleware\RedirectMiddleware;
use Laravel\Lumen\Routing\UrlGenerator;
use Mockery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\SimpleTestCase;

class RedirectMiddlewareTest extends SimpleTestCase
{
    public function testItRedirectsToDestinationWithoutPathAndParameters(): void
    {
        $url = Mockery::mock(UrlGenerator::class);
        $url->shouldReceive('to')
            ->with('/')
            ->andReturn('https://cows.com');

        $middleware = new RedirectMiddleware($url, 'sheep.com');

        $request = Request::create('https://sheep.com');

        $response = $middleware->handle($request, static function () {
        });

        $this->assertSame(301, $response->getStatusCode());
        $this->assertSame('https://cows.com', $response->headers->get('location'));
    }

    public function testItRedirectsToDestinationWithPathAndParameters(): void
    {
        $url = Mockery::mock(UrlGenerator::class);
        $url->shouldReceive('to')
            ->with('/blue/sky?tree=green')
            ->andReturn('https://cows.com/blue/sky?tree=green');

        $middleware = new RedirectMiddleware($url, 'sheep.com');

        $request = Request::create('https://sheep.com/blue/sky', parameters: [
            'tree' => 'green',
        ]);

        $response = $middleware->handle($request, static function () {
        });

        $this->assertSame(301, $response->getStatusCode());
        $this->assertSame('https://cows.com/blue/sky?tree=green', $response->headers->get('location'));
    }

    public function testItDoesNotRedirectWhenSourceDoesNotMatch(): void
    {
        $url = Mockery::mock(UrlGenerator::class);
        $url->shouldReceive('to')
            ->with('/hello/there?what=now')
            ->andReturn('https://cows.com/hello/there?what=now');

        $middleware = new RedirectMiddleware($url, 'sheep.com');

        $request = Request::create('https://elephant.com');

        $preparedResponse = new Response();
        $response = $middleware->handle($request, static fn () => $preparedResponse);

        $this->assertSame($preparedResponse, $response);
    }
}
