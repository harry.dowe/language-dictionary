<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreatePhrasesTable extends Migration
{
    public function up(): void
    {
        Schema::create('phrases', static function (Blueprint $table): void {
            $table->id();
            $table->string('phrase')->unique();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::table('phrases', static function (Blueprint $table): void {
            $table->drop();
        });
    }
}
