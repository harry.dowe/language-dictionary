<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\DictionaryEntity;
use App\Entities\PhraseEntity;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Carbon;
use stdClass;

class DatabasePhraseRepository implements PhraseRepositoryInterface
{
    public function __construct(
        private readonly ConnectionInterface $db,
    ) {
    }

    public function createPhrase(string $phrase, DictionaryEntity $dictionary): PhraseEntity
    {
        $id = $this->db->table('phrases')->insertGetId([
            'phrase' => $phrase,
            'dictionary_id' => $dictionary->getId(),
            'created_at' => Carbon::now()->toIso8601String(),
        ]);

        $entity = new PhraseEntity();
        $entity->setId($id);
        $entity->setDictionary($dictionary);
        $entity->setPhrase($phrase);

        return $entity;
    }

    /**
     * @param string[] $phrases
     *
     * @return PhraseEntity[]
     */
    public function createPhrases(array $phrases, DictionaryEntity $dictionary): array
    {
        $entities = [];
        foreach ($phrases as $phrase) {
            if (!$this->existsByParams(dictionaryId: $dictionary->getId(), phrase: $phrase)) {
                $entities[] = $this->createPhrase($phrase, $dictionary);
            }
        }

        return $entities;
    }

    public function removeById(int $id): void
    {
        $this->db->table('phrases')->delete($id);
    }

    public function updateById(int $id, string $phrase): void
    {
        $this->db->table('phrases')
            ->where('id', $id)
            ->update([
                'phrase' => $phrase,
                'updated_at' => Carbon::now()->toIso8601String(),
            ]);
    }

    /**
     * @return PhraseEntity[]
     */
    public function retrieveAll(): array
    {
        $rows = $this->db->table('phrases as p')
            ->addSelect('p.id as phrase_id')
            ->addSelect('p.phrase')
            ->addSelect('p.dictionary_id')
            ->addSelect('d.name as dictionary_name')
            ->join('dictionaries as d', 'p.dictionary_id', '=', 'd.id')
            ->get()
            ->toArray();

        return array_map(static function (stdClass $row): PhraseEntity {
            $entity = new PhraseEntity();
            $entity->setId($row->phrase_id);
            $entity->setPhrase($row->phrase);

            $dictionary = new DictionaryEntity();
            $dictionary->setId($row->dictionary_id);
            $dictionary->setName($row->dictionary_name);
            $entity->setDictionary($dictionary);

            return $entity;
        }, $rows);
    }

    public function existsByParams(
        ?int $id = null,
        ?int $exceptId = null,
        ?int $dictionaryId = null,
        ?string $phrase = null,
    ): bool {
        $qb = $this->db->table('phrases');

        if ($dictionaryId !== null) {
            $qb->where('dictionary_id', $dictionaryId);
        }

        if ($phrase !== null) {
            $qb->where('phrase', $phrase);
        }

        if ($id !== null) {
            $qb->where('id', '=', $id);
        } elseif ($exceptId !== null) {
            $qb->where('id', '!=', $exceptId);
        }

        return $qb->exists();
    }

    /**
     * @return PhraseEntity[]
     */
    public function findAllByDictionary(int $dictionary): array
    {
        $rows = $this->db->table('phrases')
            ->where('dictionary_id', $dictionary)
            ->get()
            ->toArray();

        return array_map(static function (stdClass $row): PhraseEntity {
            $entity = new PhraseEntity();
            $entity->setId($row->id);
            $entity->setPhrase($row->phrase);

            return $entity;
        }, $rows);
    }
}
