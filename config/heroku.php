<?php

declare(strict_types=1);

return [
    'url' => env('HEROKU_APP_URL'),
];
