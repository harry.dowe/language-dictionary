<?php

declare(strict_types=1);

namespace App\Validation\Request\UniqueDictionary;

use App\Repositories\DictionaryRepository;
use App\Validation\Request\RequestBodyValidatorInterface;
use Illuminate\Contracts\Validation\Validator;

class UniqueDictionary implements RequestBodyValidatorInterface
{
    public function __construct(
        private readonly DictionaryRepository $repository
    ) {
    }

    public function validate(string $attribute, mixed $value, array $customAttributes, Validator $validator): bool
    {
        return !$this->repository->existsByParams(name: $value);
    }
}
