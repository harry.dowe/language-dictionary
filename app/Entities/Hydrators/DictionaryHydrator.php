<?php

declare(strict_types=1);

namespace App\Entities\Hydrators;

use App\Entities\DictionaryEntity;
use App\Entities\UserEntity;
use stdClass;

/**
 * @psalm-type DbalDictionary = stdClass{id: int, name: string, owner_id: ?int}
 */
class DictionaryHydrator
{
    /**
     * @param DbalDictionary $data
     */
    public static function hydrateFromObject(stdClass $data): DictionaryEntity
    {
        return self::hydrate($data->id, $data->name, $data->owner_id ?? null);
    }

    public static function hydrate(int $id, string $name, ?int $ownerId = null): DictionaryEntity
    {
        $dictionary = new DictionaryEntity();
        $dictionary->setId($id);
        $dictionary->setName($name);

        if ($ownerId !== null) {
            $owner = new UserEntity();
            $owner->setId($ownerId);
            $dictionary->setOwner($owner);
        }

        return $dictionary;
    }

    /**
     * @param DbalDictionary[] $collection
     *
     * @return DictionaryEntity[]
     */
    public static function hydrateFromArray(array $dictionaries): array
    {
        return array_map(static function (stdClass $dictionary): DictionaryEntity {
            return self::hydrateFromObject($dictionary);
        }, $dictionaries);
    }
}
