<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class AlterPhrasesPhraseLength extends Migration
{
    public function up(): void
    {
        Schema::table('phrases', static function (Blueprint $table): void {
            $table->string('phrase', 30)->change();
        });
    }

    public function down(): void
    {
        Schema::create('phrases', static function (Blueprint $table): void {
            $table->string('phrase', 255)->change();
        });
    }
}
