<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="A searchable dictionary of phrases">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Dictionary - Registration</title>
</head>
<body>
<script>
function submitUser() {
    fetch('{{ route('api.users.create') }}', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            username: document.getElementById('username').value,
            password: document.getElementById('password').value,
        }),
    })
        .then(handleErrors)
        .then(() => {
            alert('User created!');
        })
        .catch(error => {
            alert(`Error ${error.message}`);
        });

    return false;
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }

    return response;
}
</script>

<h1>Language Dictionary</h1>
<h2>Registration</h2>
@if ($canCreateUsers)
    <form action="" method="post" onsubmit="return submitUser()">
        <label for="username">Username</label>
        <input type="text" name="username" id="username"/>

        <label for="password">Password</label>
        <input type="password" name="password" id="password"/>

        <button type="submit">Submit</button>
    </form>
@else
    <div>Registration is disabled.</div>
@endif
</body>
</html>
