<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\PhraseEntity;
use App\Repositories\DictionaryRepository;
use App\Repositories\PhraseRepository;
use App\Utils\StringValue;
use App\Validation\Owners\EntityOwner;
use App\Validation\Uri\DictionaryExists;
use App\Validation\Uri\PhraseExists;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Contracts\JWTSubject;

final class PhraseController extends Controller
{
    public function __construct(
        private PhraseRepository $repository,
    ) {
    }

    public function index(Request $request, string $id, DictionaryExists $validator): JsonResponse
    {
        if (!$validator->validate($id)) {
            throw new NotFoundHttpException('Dictionary not found.');
        }

        $phrases = array_map(static function (PhraseEntity $entity): string {
            return $entity->getPhrase();
        }, $this->repository->findAllByDictionary((int) $id));

        return new JsonResponse([
            'phrases' => $phrases,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     * @throws ValidationException
     */
    public function createSingle(
        Request $request,
        string $id,
        JWTSubject $user,
        DictionaryExists $validator,
        DictionaryRepository $repository,
    ): Response {
        if (!$validator->validate($id)) {
            throw new NotFoundHttpException('Dictionary not found.');
        }

        $dictionaryEntity = $repository->findById((int) $id);

        if (!EntityOwner::ownsEntity($user, $dictionaryEntity)) {
            throw new AccessDeniedHttpException('Does not own dictionary.');
        }

        $this->getValidationFactory()->make($request->json()?->all() ?? [], [
            'phrase' => ['required', 'string', 'max:30', "unique_phrase:{$id}"],
        ])->validate();

        $phrase = StringValue::toTrimLower($request->json('phrase'));
        $this->repository->createPhrase($phrase, $dictionaryEntity);

        return new Response(status: Response::HTTP_CREATED);
    }

    /**
     * @throws NotFoundHttpException
     * @throws ValidationException
     */
    public function createMultiple(
        Request $request,
        string $id,
        JWTSubject $user,
        DictionaryExists $validator,
        DictionaryRepository $repository,
    ): Response {
        if (!$validator->validate($id)) {
            throw new NotFoundHttpException('Dictionary not found.');
        }

        $dictionaryEntity = $repository->findById((int) $id);

        if (!EntityOwner::ownsEntity($user, $dictionaryEntity)) {
            throw new AccessDeniedHttpException('Does not own dictionary.');
        }

        $data = $request->json()?->all() ?? [];

        $this->getValidationFactory()->make($data, [
            'phrases' => ['required', 'array', 'min:1'],
            'phrases.*.phrase' => ['string', 'required', 'max:30'],
        ])->validate();

        $phrases = (new Collection($data['phrases']))
            ->unique()
            ->map(static fn (array $phrase): string => StringValue::toTrimLower($phrase['phrase']));

        $this->repository->createPhrases($phrases->toArray(), $dictionaryEntity);

        return new Response(status: Response::HTTP_CREATED);
    }

    /**
     * @throws NotFoundHttpException
     * @throws ValidationException
     */
    public function update(
        Request $request,
        string $id,
        JWTSubject $user,
        PhraseExists $validator,
        PhraseRepository $phraseRepository,
    ): JsonResponse {
        if (!$validator->validate($id)) {
            throw new NotFoundHttpException('Phrase not found.');
        }

        $phraseEntity = $phraseRepository->findById((int) $id);

        if (!EntityOwner::ownsEntity($user, $phraseEntity->getDictionary())) {
            throw new AccessDeniedHttpException('Does not own dictionary.');
        }

        $this->getValidationFactory()->make($request->json()?->all() ?? [], [
            'phrase' => ['required', 'string', 'max:255', "unique_phrase:,{$id}"],
        ])->validate();

        $phrase = StringValue::toTrimLower($request->json('phrase'));

        $this->repository->updateById((int) $id, $phrase);

        return new JsonResponse([
            'id' => (int) $id,
            'phrase' => $phrase,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function delete(
        Request $request,
        string $id,
        JWTSubject $user,
        PhraseExists $validator,
        PhraseRepository $phraseRepository,
    ): Response {
        if (!$validator->validate($id)) {
            throw new NotFoundHttpException('Phrase not found.');
        }

        $phraseEntity = $phraseRepository->findById((int) $id);

        if (!EntityOwner::ownsEntity($user, $phraseEntity->getDictionary())) {
            throw new AccessDeniedHttpException('Does not own dictionary.');
        }

        $this->repository->removeById((int) $id);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
