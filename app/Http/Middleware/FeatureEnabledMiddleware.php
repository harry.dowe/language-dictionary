<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\FeatureToggle\Feature;
use App\FeatureToggle\FeatureEnabledService;
use Closure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

final class FeatureEnabledMiddleware
{
    public function __construct(
        private readonly FeatureEnabledService $service,
    ) {
    }

    public function handle(Request $request, Closure $next, string $feature): Response
    {
        if (!$this->service->featureEnabled(Feature::from($feature))) {
            throw new AccessDeniedHttpException($feature);
        }

        return $next($request);
    }
}
