<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Entities\DictionaryEntity;
use App\Repositories\DictionaryRepository;
use Illuminate\Console\Command;

class ListDictionariesCommand extends Command
{
    private const HEADERS = [
        'dictionary_id',
        'dictionary_name',
        'owner',
    ];

    protected $signature = 'admin:dictionaries:list';

    public function __construct(
        private readonly DictionaryRepository $repository
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $dictionaries = array_map(static fn (DictionaryEntity $dictionary): array => [
            'dictionary_id' => $dictionary->getId(),
            'dictionary_name' => $dictionary->getName(),
            'owner' => $dictionary->getOwner()?->getUsername(),
        ], $this->repository->getDictionaries());

        $this->table(self::HEADERS, $dictionaries, 'box');
    }
}
