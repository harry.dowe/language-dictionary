<?php

declare(strict_types=1);

namespace Tests\Unit\App\Validation\Owners;

use App\Auth\User;
use App\Entities\Interfaces\OwnsResource;
use App\Entities\UserEntity;
use App\Validation\Owners\EntityOwner;
use Tests\SimpleTestCase;

class EntityOwnerTest extends SimpleTestCase
{
    public function testOwnsEntityWhenNotAdmin(): void
    {
        $entity = $this->buildOwner(1);

        $user = new User(1, 'pass', false);

        $this->assertTrue(EntityOwner::ownsEntity($user, $entity));
    }

    public function testAdminOwnsEntity(): void
    {
        $entity = $this->buildOwner(4);

        $user = new User(1, 'pass', true);

        $this->assertTrue(EntityOwner::ownsEntity($user, $entity));
    }

    public function testDoesNotOwnEntity(): void
    {
        $entity = $this->buildOwner(4);

        $user = new User(1, 'pass', false);

        $this->assertFalse(EntityOwner::ownsEntity($user, $entity));
    }

    public function testDoesNotRequireOwnershipWhenNullOwner(): void
    {
        $entity = $this->buildOwner(null);

        $user = new User(4, 'pass', true);

        $this->assertTrue(EntityOwner::ownsEntity($user, $entity));
    }

    private function buildOwner(?int $ownerId): OwnsResource
    {
        $owner = null;
        if ($ownerId !== null) {
            $owner = new UserEntity();
            $owner->setId($ownerId);
        }

        return new class($owner) implements OwnsResource {
            private ?UserEntity $owner;

            public function __construct(?UserEntity $owner)
            {
                $this->owner = $owner;
            }

            public function getOwner(): ?UserEntity
            {
                return $this->owner;
            }
        };
    }
}
