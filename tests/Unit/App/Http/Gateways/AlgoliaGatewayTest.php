<?php

declare(strict_types=1);

namespace Tests\Unit\App\Http\Gateways;

use Algolia\AlgoliaSearch\SearchClient;
use Algolia\AlgoliaSearch\SearchIndex;
use App\Entities\DictionaryEntity;
use App\Entities\PhraseEntity;
use App\Http\Gateways\AlgoliaGateway;
use Illuminate\Support\Carbon;
use Mockery;
use Tests\SimpleTestCase;

/**
 * @covers \App\Http\Gateways\AlgoliaGateway
 */
class AlgoliaGatewayTest extends SimpleTestCase
{
    public function testAddSingleRecord(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1615282471));

        $index = Mockery::mock(SearchIndex::class);
        $index->shouldReceive('saveObject')->with([
            'objectID' => 34,
            'phrase' => 'baaa',
            'dictionary' => 'sheep',
            'createdAt' => 1615282471,
        ]);

        $client = Mockery::mock(SearchClient::class);
        $client->shouldReceive('initIndex')->with('animals')->andReturn($index);

        $dictionaryEntity = new DictionaryEntity();
        $dictionaryEntity->setId(55);
        $dictionaryEntity->setName('sheep');

        $entity = new PhraseEntity();
        $entity->setPhrase('baaa');
        $entity->setId(34);
        $entity->setDictionary($dictionaryEntity);

        $gateway = new AlgoliaGateway($client);
        $gateway->addRecord('animals', $entity);
    }

    public function testAddMultipleRecords(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1615282471));

        $sheepDictionary = new DictionaryEntity();
        $sheepDictionary->setName('Sheep');

        $sheep = new PhraseEntity();
        $sheep->setId(56);
        $sheep->setPhrase('baa');
        $sheep->setDictionary($sheepDictionary);

        $cowDictionary = new DictionaryEntity();
        $cowDictionary->setName('Cows');
        $cow = new PhraseEntity();
        $cow->setId(72);
        $cow->setPhrase('moo');
        $cow->setDictionary($cowDictionary);

        $index = Mockery::mock(SearchIndex::class);
        $index->shouldReceive('saveObjects')->with([
            [
                'objectID' => 56,
                'phrase' => 'baa',
                'dictionary' => 'Sheep',
                'createdAt' => 1615282471,
            ],
            [
                'objectID' => 72,
                'phrase' => 'moo',
                'dictionary' => 'Cows',
                'createdAt' => 1615282471,
            ],
        ]);

        $client = Mockery::mock(SearchClient::class);
        $client->shouldReceive('initIndex')->with('animals')->andReturn($index);

        $gateway = new AlgoliaGateway($client);
        $gateway->addRecords('animals', [
            $sheep,
            $cow,
        ]);
    }

    public function testDeleteObject(): void
    {
        $index = Mockery::mock(SearchIndex::class);
        $index->shouldReceive('deleteObject')->with(34);

        $client = Mockery::mock(SearchClient::class);
        $client->shouldReceive('initIndex')->with('animals')->andReturn($index);

        $gateway = new AlgoliaGateway($client);
        $gateway->deleteObject('animals', 34);
    }

    public function testClearObjects(): void
    {
        $index = Mockery::mock(SearchIndex::class);
        $index->shouldReceive('clearObjects');

        $client = Mockery::mock(SearchClient::class);
        $client->shouldReceive('initIndex')->with('animals')->andReturn($index);

        $gateway = new AlgoliaGateway($client);
        $gateway->clearObjects('animals');
    }

    public function testUpdateRecord(): void
    {
        $index = Mockery::mock(SearchIndex::class);
        $index->shouldReceive('partialUpdateObject')->with([
            'objectID' => 34,
            'phrase' => 'baaa',
        ]);

        $client = Mockery::mock(SearchClient::class);
        $client->shouldReceive('initIndex')->with('animals')->andReturn($index);

        $gateway = new AlgoliaGateway($client);
        $gateway->updateRecord('animals', 34, 'baaa');
    }
}
