<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

final class DictionarySeeder extends Seeder
{
    private readonly string $now;

    public function __construct()
    {
        $this->now = Carbon::now()->toIso8601String();
    }

    public function run(): void
    {
        DB::table('dictionaries')
            ->insert([
                'name' => 'Spanish',
                'created_at' => $this->now,
            ]);

        DB::table('dictionaries')
            ->insert([
                'name' => 'English',
                'created_at' => $this->now,
            ]);
    }
}
