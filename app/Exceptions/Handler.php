<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

final class Handler extends ExceptionHandler
{
    /**
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * @return Response
     */
    public function render($request, Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof HttpResponseException) {
            return $exception->getResponse();
        }

        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception);
        }

        if ($request->acceptsHtml()) {
            return $this->prepareResponse($request, $exception);
        }

        return $this->prepareJsonResponse($request, $exception);
    }

    protected function prepareException(
        Throwable $exception,
    ): Throwable | NotFoundHttpException | AccessDeniedHttpException | HttpException {
        if ($exception instanceof ModelNotFoundException) {
            return new NotFoundHttpException($exception->getMessage(), $exception);
        }

        if ($exception instanceof AuthorizationException) {
            return new AccessDeniedHttpException($exception->getMessage(), $exception);
        }

        return $exception;
    }

    protected function convertValidationExceptionToResponse(ValidationException $exception): Response
    {
        if ($exception->response) {
            return $exception->response;
        }

        return new JsonResponse([
            'message' => $exception->getMessage(),
            'errors' => $exception->errors(),
        ], $exception->status);
    }

    protected function convertExceptionToArray(Throwable $e): array
    {
        if (config('app.debug')) {
            return [
                'message' => $e->getMessage(),
                'exception' => $e::class,
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => array_map(static fn (array $trace): array => Arr::except($trace, ['args']), $e->getTrace()),
            ];
        }

        return [
            'message' => $this->isHttpException($e) ? $e->getMessage() : 'Server Error',
        ];
    }
}
