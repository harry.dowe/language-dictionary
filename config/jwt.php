<?php

declare(strict_types=1);

return [
    'secret' => env('JWT_SECRET'),
    'blacklist_enabled' => false,
    'ttl' => 20160, // Two weeks
    'refresh_ttl' => 20160,
    'algo' => 'HS256',
    'providers' => [
        'jwt' => Tymon\JWTAuth\Providers\JWT\Lcobucci::class,
        'auth' => Tymon\JWTAuth\Providers\Auth\Illuminate::class,
        'storage' => Tymon\JWTAuth\Providers\Storage\Illuminate::class,
    ],
];
