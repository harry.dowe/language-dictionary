<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\Hydrators\DateTimeHydrator;
use App\Entities\Hydrators\UserHydrator;
use App\Entities\UserEntity;
use Carbon\Carbon;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\ConnectionInterface;
use stdClass;

class UserRepository
{
    public function __construct(
        private readonly ConnectionInterface $db,
        private readonly Hasher $hasher,
    ) {
    }

    public function createUser(string $username, string $password): void
    {
        $this->db->table('users')
            ->insert([
                'username' => $username,
                'password' => $this->hasher->make($password),
                'created_at' => Carbon::now()->toIso8601String(),
            ]);
    }

    /**
     * @return UserEntity[]
     */
    public function getUsers(): array
    {
        return $this->db->table('users')
            ->orderBy('created_at')
            ->get()
            ->map(static function (stdClass $row): UserEntity {
                return UserHydrator::hydrate(
                    $row->id,
                    $row->username,
                    $row->password,
                    $row->is_admin,
                    DateTimeHydrator::hydrate($row->created_at),
                    DateTimeHydrator::hydrate($row->updated_at),
                );
            })->toArray();
    }
}
