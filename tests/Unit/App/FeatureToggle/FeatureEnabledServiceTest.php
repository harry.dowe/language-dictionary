<?php

declare(strict_types=1);

namespace Tests\Unit\App\FeatureToggle;

use App\FeatureToggle\FeatureEnabledService;
use Illuminate\Config\Repository as Config;
use Tests\SimpleTestCase;
use UnexpectedValueException;

class FeatureEnabledServiceTest extends SimpleTestCase
{
    public function testItReturnsCanCreateUsers(): void
    {
        $config = new Config([
            'features' => [
                'users' => [
                    'create' => true,
                ],
            ],
        ]);
        $service = new FeatureEnabledService($config);

        $this->assertTrue($service->canCreateUsers());
    }

    public function testItReturnsCannotCreateUsers(): void
    {
        $config = new Config([
            'features' => [
                'users' => [
                    'create' => false,
                ],
            ],
        ]);
        $service = new FeatureEnabledService($config);

        $this->assertFalse($service->canCreateUsers());
    }

    public function testItThrowsUnexpectedValue(): void
    {
        $service = new FeatureEnabledService(new Config());

        $this->expectException(UnexpectedValueException::class);
        $service->canCreateUsers();
    }
}
