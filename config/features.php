<?php

declare(strict_types=1);

return [
    'users' => [
        'create' => env('CREATE_USERS_ENABLED', false),
    ],
];
