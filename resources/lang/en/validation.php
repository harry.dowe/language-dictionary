<?php

declare(strict_types=1);

return [
    'unique_dictionary' => 'Dictionary already exists',
    'unique_phrase' => 'Phrase already exists',
];
