<?php

declare(strict_types=1);

namespace App\Exceptions\Entities;

use RuntimeException;

class EntityNotFoundException extends RuntimeException
{
}
