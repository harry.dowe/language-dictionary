<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Entities\DictionaryEntity;

class DictionaryTransformer
{
    public static function extract(DictionaryEntity $dictionary): array
    {
        return [
            'id' => $dictionary->getId(),
            'name' => $dictionary->getName(),
            'owner' => $dictionary->getOwner()?->getId(),
        ];
    }
}
