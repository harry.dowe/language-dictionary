<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="A searchable dictionary of phrases">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Dictionary</title>
    <link rel="stylesheet" type="text/css" href="{{ url() }}/css/app.css">
</head>
<body>
<div id="app"></div>
<script src="{{ url() }}/js/app.js"></script>
</body>
</html>
