<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Http\Gateways\AlgoliaGateway;
use App\Repositories\DatabasePhraseRepository;
use Illuminate\Console\Command;

final class SyncPhrasesCommand extends Command
{
    protected $signature = 'sync';

    public function __construct(
        private readonly DatabasePhraseRepository $db,
        private readonly AlgoliaGateway $algolia,
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->output->writeln('Syncing data from db to Algolia...');

        $entities = $this->db->retrieveAll();

        $this->algolia->clearObjects('dictionary');

        $this->algolia->addRecords('dictionary', $entities);

        $this->output->writeln('Complete!');
    }
}
