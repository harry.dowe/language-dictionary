<?php

declare(strict_types=1);

/** @var Laravel\Lumen\Routing\Router $router */

/** @see Appcontroller::hello() */

use App\FeatureToggle\Feature;
use Laravel\Lumen\Routing\Router;

$router->get('/', [
    'as' => 'app.home',
    'uses' => 'AppController@hello',
]);

$router->get('register', [
    'as' => 'app.register',
    'uses' => 'AppController@register',
]);

$router->post('api/users', [
    'as' => 'api.users.create',
    'uses' => 'UserController@create',
    'middleware' => 'feature:' . Feature::CreateUsers->value,
]);

/** @see App\Http\Controllers\DictionaryController::index() */
$router->get('api/dictionaries', [
    'as' => 'api.dictionary.index',
    'uses' => 'DictionaryController@index',
]);

/** @see App\Http\Controllers\DictionaryController::create() */
$router->post('api/dictionaries', [
    'as' => 'api.dictionary.create',
    'uses' => 'DictionaryController@create',
    'middleware' => 'admin',
]);

/** @see App\Http\Controllers\PhraseController::index() */
$router->get('api/dictionaries/{id:\d+}/phrases', [
    'as' => 'api.phrase.index',
    'uses' => 'PhraseController@index',
]);

/** @see App\Http\Controllers\PhraseController::createSingle() */
$router->post('api/dictionaries/{id:\d+}/phrase', [
    'as' => 'api.phrase.create',
    'uses' => 'PhraseController@createSingle',
    'middleware' => 'jwt.auth',
]);

/** @see App\Http\Controllers\PhraseController::createMultiple() */
$router->post('api/dictionaries/{id:\d+}/phrases', [
    'as' => 'api.phrase.create.multiple',
    'uses' => 'PhraseController@createMultiple',
    'middleware' => 'jwt.auth',
]);

/** @see App\Http\Controllers\PhraseController::update() */
$router->patch('api/phrases/{id:\d+}', [
    'as' => 'api.phrase.update',
    'uses' => 'PhraseController@update',
    'middleware' => 'jwt.auth',
]);

/** @see App\Http\Controllers\PhraseController::delete() */
$router->delete('api/phrases/{id:\d+}', [
    'as' => 'api.phrase.delete',
    'uses' => 'PhraseController@delete',
    'middleware' => 'jwt.auth',
]);

$router->group([
    'prefix' => 'auth',
], static function (Router $router): void {
    $router->post('login', [
        'as' => 'auth.login',
        'uses' => 'AuthController@login',
    ]);

    $router->post('refresh', [
        'as' => 'auth.refresh',
        'uses' => 'AuthController@refresh',
    ]);
});
