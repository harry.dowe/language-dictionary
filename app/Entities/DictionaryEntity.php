<?php

declare(strict_types=1);

namespace App\Entities;

use App\Entities\Interfaces\OwnsResource;

class DictionaryEntity implements OwnsResource
{
    private int $id;

    private string $name;

    private ?UserEntity $owner = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getOwner(): ?UserEntity
    {
        return $this->owner;
    }

    public function setOwner(?UserEntity $owner): void
    {
        $this->owner = $owner;
    }
}
