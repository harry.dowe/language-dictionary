<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Algolia\AlgoliaSearch\SearchClient;
use Illuminate\Console\Command;

final class CopyIndexSettingsCommand extends Command
{
    protected $signature = 'algolia:settings:copy {from} {to}';

    protected $description = 'Copy configuration from one index to another.';

    public function __construct(
        private readonly SearchClient $algolia
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $from = $this->input->getArgument('from');
        $to = $this->input->getArgument('to');

        $this->output->writeln("Copying setting from `{$from}` to `{$to}`.");

        $this->algolia->copySettings($from, $to);

        $this->output->writeln('Operation complete!');
    }
}
