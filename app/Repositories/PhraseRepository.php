<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\DictionaryEntity;
use App\Entities\Hydrators\DictionaryHydrator;
use App\Entities\PhraseEntity;
use App\Exceptions\Entities\PhraseEntityNotFoundException;
use Illuminate\Database\ConnectionInterface;
use Throwable;

class PhraseRepository implements PhraseRepositoryInterface
{
    public function __construct(
        private readonly ConnectionInterface $db,
        private readonly AlgoliaPhraseRepository $algoliaRepository,
        private readonly DatabasePhraseRepository $repository,
    ) {
    }

    public function createPhrase(string $phrase, DictionaryEntity $dictionary): PhraseEntity
    {
        $this->db->beginTransaction();

        try {
            $entity = $this->repository->createPhrase($phrase, $dictionary);
            $this->algoliaRepository->createPhrase($entity);

            $this->db->commit();
        } catch (Throwable $exception) {
            $this->db->rollBack();

            throw $exception;
        }

        return $entity;
    }

    public function removeById(int $id): void
    {
        $this->db->beginTransaction();

        try {
            $this->repository->removeById($id);
            $this->algoliaRepository->removeById($id);

            $this->db->commit();
        } catch (Throwable $exception) {
            $this->db->rollBack();

            throw $exception;
        }
    }

    public function updateById(int $id, string $phrase): void
    {
        $this->db->beginTransaction();

        try {
            $this->repository->updateById($id, $phrase);
            $this->algoliaRepository->updateById($id, $phrase);

            $this->db->commit();
        } catch (Throwable $exception) {
            $this->db->rollBack();

            throw $exception;
        }
    }

    /**
     * @return PhraseEntity[]
     */
    public function findAllByDictionary(int $dictionary): array
    {
        return $this->repository->findAllByDictionary($dictionary);
    }

    /**
     * @return PhraseEntity[]
     */
    public function createPhrases(array $phrases, DictionaryEntity $dictionary): array
    {
        $this->db->beginTransaction();

        try {
            $entities = $this->repository->createPhrases($phrases, $dictionary);
            $this->algoliaRepository->createPhrases($entities);

            $this->db->commit();
        } catch (Throwable $exception) {
            $this->db->rollBack();

            throw $exception;
        }

        return $entities;
    }

    public function findById(int $id): PhraseEntity
    {
        $phrase = $this->db->table('phrases as p')
            ->join('dictionaries as d', 'p.dictionary_id', '=', 'd.id')
            ->where('p.id', $id)
            ->first([
                'p.phrase as phrase',
                'd.id as dictionary_id',
                'd.name as dictionary_name',
                'd.owner_id as dictionary_owner',
            ]);

        if ($phrase === null) {
            throw new PhraseEntityNotFoundException("Phrase with id `{$id}` not found.");
        }

        $phraseEntity = new PhraseEntity();
        $phraseEntity->setId($id);
        $phraseEntity->setPhrase($phrase->phrase);
        $phraseEntity->setDictionary(DictionaryHydrator::hydrate(
            $phrase->dictionary_id,
            $phrase->dictionary_name,
            $phrase->dictionary_owner,
        ));

        return $phraseEntity;
    }
}
