<?php

declare(strict_types=1);

namespace App\Validation\Request;

use App\Repositories\DatabasePhraseRepository;
use App\Utils\StringValue;
use Illuminate\Contracts\Validation\Validator;

class UniquePhrase implements RequestBodyValidatorInterface
{
    public function __construct(
        private DatabasePhraseRepository $repository,
    ) {
    }

    public function validate(string $attribute, mixed $value, array $customAttributes, Validator $validator): bool
    {
        return !$this->repository->existsByParams(
            exceptId: $this->getId($customAttributes),
            dictionaryId: $this->getDictionary($customAttributes),
            phrase: StringValue::toTrimLower($value),
        );
    }

    private function getId(array $customAttributes): ?int
    {
        if (empty($customAttributes[1])) {
            return null;
        }

        return (int) $customAttributes[1];
    }

    private function getDictionary(array $customAttributes): ?int
    {
        if (empty($customAttributes[0])) {
            return null;
        }

        return (int) $customAttributes[0];
    }
}
