<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Algolia\AlgoliaSearch\SearchClient;
use Illuminate\Console\Command;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Carbon;

final class ImportPhrasesCommand extends Command
{
    protected $signature = 'import';

    public function __construct(
        private readonly ConnectionInterface $db,
        private readonly SearchClient $algolia,
        private readonly string $indexPrefix
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->output->writeln('Importing data from Algolia to db...');

        $objects = $this->algolia->initIndex($this->buildIndex('dictionary'))->browseObjects();

        $dictionaries = $this->getDictionaries();

        foreach ($objects as $object) {
            $this->db->table('phrases')->insertGetId([
                'phrase' => $object['phrase'],
                'dictionary_id' => $dictionaries[$object['dictionary']],
                'created_at' => $this->getCreatedAt($object),
            ]);
        }

        $this->output->writeln('Complete!');
    }

    private function buildIndex(string $index): string
    {
        if ($this->indexPrefix === null) {
            return $index;
        }

        return "{$this->indexPrefix}_{$index}";
    }

    private function getCreatedAt(array $object): string
    {
        if (isset($object['createdAt'])) {
            return Carbon::createFromTimestamp($object['createdAt'])->toIso8601String();
        }

        return Carbon::now()->toIso8601String();
    }

    /**
     * @return array<string, int>
     */
    private function getDictionaries(): array
    {
        $results = $this->db->table('dictionaries')
            ->get(['id', 'name'])
            ->toArray();

        $dictionaries = [];
        foreach ($results as $result) {
            $dictionaries[$result->name] = $result->id;
        }

        return $dictionaries;
    }
}
