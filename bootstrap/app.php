<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__),
))->bootstrap();

date_default_timezone_set('UTC');

$app = new Laravel\Lumen\Application(dirname(__DIR__));

$app->withFacades();
$app->withEloquent();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class,
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class,
);

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);

$app->middleware([
    App\Http\Middleware\RedirectMiddleware::class,
    App\Http\Middleware\CorsMiddleware::class,
]);

$app->routeMiddleware([
    'feature' => App\Http\Middleware\FeatureEnabledMiddleware::class,
    'admin' => App\Http\Middleware\RequiresAdminMiddleware::class,
]);

$app->configure('app');
$app->configure('algolia');
$app->configure('logging');
$app->configure('jwt');
$app->configure('auth');
$app->configure('database');
$app->configure('features');
$app->configure('heroku');

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], static function (Laravel\Lumen\Routing\Router $router): void {
    require __DIR__ . '/../routes/web.php';
});

return $app;
