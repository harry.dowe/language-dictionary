<?php

declare(strict_types=1);

namespace App\Entities\Hydrators;

use Carbon\Carbon;
use DateTimeInterface;

class DateTimeHydrator
{
    public static function hydrate(?string $dateTime): ?DateTimeInterface
    {
        if ($dateTime === null) {
            return null;
        }

        return Carbon::createFromFormat('Y-m-d H:i:s', $dateTime);
    }
}
