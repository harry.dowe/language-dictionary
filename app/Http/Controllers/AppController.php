<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\FeatureToggle\FeatureEnabledService;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\View\View;

final class AppController extends Controller
{
    public function __construct(
        private readonly ViewFactory $viewFactory
    ) {
    }

    public function hello(): View
    {
        return $this->viewFactory->make('app');
    }

    public function register(FeatureEnabledService $service): View
    {
        return $this->view->viewFactory->make('register', [
            'canCreateUsers' => $service->canCreateUsers(),
        ]);
    }
}
