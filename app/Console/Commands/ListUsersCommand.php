<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Entities\UserEntity;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;

class ListUsersCommand extends Command
{
    protected $signature = 'admin:users:list';

    private const HEADERS = [
        'id',
        'username',
        'is_admin',
        'created_at',
        'updated_at',
    ];

    public function __construct(
        private readonly UserRepository $repository,
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $users = array_map(static fn(UserEntity $user): array => [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'is_admin' => $user->isAdmin() ? 'true' : 'false',
            'created_at' => $user->getCreatedAt()?->format('Y-m-d H:i:s'),
            'updated_at' => $user->getUpdatedAt()?->format('Y-m-d H:i:s'),
        ], $this->repository->getUsers());

        $this->table(self::HEADERS, $users, 'box');
    }
}
