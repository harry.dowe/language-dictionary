<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class CorsMiddleware
{
    private const HEADERS = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
        'Access-Control-Allow-Credentials' => 'true',
        'Access-Control-Max-Age' => '86400',
        'Access-Control-Allow-Headers' => 'Content-Type, Authorization, X-Requested-With',
    ];

    public function handle(Request $request, Closure $next): JsonResponse | Response
    {
        if ($request->isMethod('OPTIONS')) {
            return new JsonResponse(['method' => 'OPTIONS'], Response::HTTP_OK, self::HEADERS);
        }

        /** @var Response $response */
        $response = $next($request);

        $response->headers->add(self::HEADERS);

        return $response;
    }
}
